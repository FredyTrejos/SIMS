<?php
//error_reporting(E_ALL && ~E_NOTICE);
session_start();//entrar solo iniciando sesión



if(isset($_SESSION['ctrlogA']) && ($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr")){

    require_once("../clases/frontEnd.php");
    require_once("../clases/usuarios.php");
    require_once("../clases/equipos.php");
    require_once("../clases/mantenimientos.php");


    $obj1 = new frontEnd();
    $obj2 = new usuarios();
    $obj3 = new equipos();
    $obj4 = new mantenimientos();


    $obj1->encabezado("../");
    $obj1->menuAdmin();

    switch ( $_GET["opc"]){
        case '11': //Se carga el listado de usuario
                    $obj2->listado();
                    break;
        case '12':  //agregar usuario
                    $obj2->fagregar();
                    break;
        case '13': //consultar usuario
                    $obj2->consultar($_GET["id"]);
                    break;
        case '14': //modificar usuario
                    $obj2->fmodificar($_GET["id"]);
                    break;
        case '15': //eliminar usuario
                    $obj2->feliminar($_GET["id"]);
                    break;
        case '21': //Se carga el listado equipos
                    $obj3->listadoequipos($_SESSION["id"]);
                    break;
        case '22':  //agregar equipo
                    $obj3->fagregarequipos();
                    break;
        case '23': //consultar equipo
                    $obj3->consultarequipos($_GET["id"]);
                    break;
        case '24': //modificar equipo
                    $obj3->fmodificarequipos($_GET["id"]);
                    break;
        case '25': //eliminar equipo
                    $obj3->feliminarequipos($_GET["id"]);
                    break;
        case '31': //Se carga el listado mantenimientos
                    $obj4->listadomantenimientos($_GET["id"]);
                    break;
        case '32':  //agregar mantenimientos
                    $obj4->fagregarmantenimientos($_GET["id"]);
                    break;
        case '33': //consultar mantenimientos
                    $obj4->consultarmantenimientos($_GET["idu"],$_GET["ide"],$_GET["fe"]);
                    break;
        case '34': //modificar mantenimientos
                    $obj4->fmodificarmantenimientos($_GET["idu"],$_GET["ide"],$_GET["fe"]);
                    break;
        case '35': //eliminar mantenimientos
                    $obj4->feliminarmantenimientos($_GET["idu"],$_GET["ide"],$_GET["fe"]);
                    break;
        case '41': //perfil actualizar
                    $obj1->actualizarPerfil($_SESSION["id"]);
                    break;
        case '99': //cerrar sesion
                    $_SESSION= array();
                    echo "<script>window.location='../index.php?opc=1';</script>";
    }

    $obj1->piePagina();

    unset( $obj1 );
    unset( $obj2 );
    unset( $obj3 );
    unset( $obj4 );
    unset( $obj5 );
}
else{
    echo "<link rel='stylesheet' href='../scripts/style.css'>";
    echo "<h1 class='warning'>¡Acceso Restringido!</h1>";
    echo "<h2 class='warning'>¡Usted está intentando acceder a un sitio sin estar autorizado!</h2>";
}


?>