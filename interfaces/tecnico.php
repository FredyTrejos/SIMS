<?php
session_start();//entrar solo iniciando sesión
//error_reporting(E_ALL && ~E_NOTICE);

if(isset($_SESSION['ctrlogT']) && ($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn")){


    require_once("../clases/frontEnd.php");
    require_once("../clases/equipos.php");
    require_once("../clases/mantenimientos.php");


    $obj1 = new frontEnd();
    $obj3 = new equipos();
    $obj4 = new mantenimientos();


    $obj1->encabezado("../");
    $obj1->menuUser();

    switch ( $_GET["opc"]){
        case '21': //Se carga el listado de equipos
            $obj3->listadoequipos($_SESSION["ti"]);
            break;
        case '22': //Se carga el listado de equipos
            $obj3->fagregarequipos();
            break;
        case '23': //consultar equipos
            $obj3->consultarequipos($_GET["id"]);
            break;
        case '31':  //listado de mantenimientos
            $obj4->listadomantenimientos($_GET["id"]);
            break;  
        case '32':  //agregar mantenimientos
            $obj4->fagregarmantenimientos($_GET["id"]);
            break; 
        case '33': //consultar mantenimientos
            $obj4->consultarmantenimientos($_GET["idu"],$_GET["ide"],$_GET["fe"]);
            break; 
        case '41': //perfil actualizar
            $obj1->actualizarPerfil($_SESSION["id"]);
            break;
        case '99': //cerrar sesion
            $_SESSION= array();
            echo "<script>window.location='../index.php?opc=1';</script>";
    }


    $obj1->piePagina();


    unset( $obj1 );
    unset( $obj3 );
    unset( $obj4 );

}
else{
    echo "<link rel='stylesheet' href='../scripts/style.css'>";
    echo "<h1 class='warning'>¡Acceso Restringido!</h1>";
    echo "<h2 class='warning'>¡Usted está intentando acceder a un sitio sin estar autorizado!</h2>";
}
?>