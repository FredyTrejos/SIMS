// **************************************************
// Custom code for UD09Form
// Created: 02/08/2016 05:34:05 p.m.
// **************************************************
// 2017/03/03 DAF Inicio
// **************************************************
// 10/MAR/2017 AZAMBRANO: Se modifica el metodo ConsultarInformacionOTSaImprimir para adicionar
//						una columna de numero de impresiones deseadas de una OT
// actualización en este lugar
// **************************************************


#region DECLARACIÓN DE USING

using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using Ice.BO;
using Ice.UI;
using Ice.Lib.Customization;
using Ice.Lib.ExtendedProps;
using Ice.Lib.Framework;
using Ice.Lib.Searches;
using Ice.UI.FormFunctions;
using DFM_Utilidades;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using Ice.Adapters;
using System.IO;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;


#endregion

public class Script
{
	#region DECLARACIÓN DE VARIABLES Y CONSTANTES
	
	
	// ** Wizard Insert Location - Do Not Remove 'Begin/End Wizard Added Module Level Variables' Comments! **
	// Begin Wizard Added Module Level Variables **

	// End Wizard Added Module Level Variables **

	// Add Custom Module Level Variables Here **
	
	private string mCurrentCompany;
	private string mUsuario;  
	
	private const string NOMBRE_BAQ = "B_RPPro_ROTB_V2"; 
	private const string CAMPO_LISTAOT = "ListaOT";	
	private const string MENSAJE_ERROR_DATOS_BAQ = "Ocurrio un error al consultar los datos.";
	private const string MENSAJE_CADENA_VACIA = "Por favor ingrese al menos una OT.";
	private const string MENSAJE_CARGO_DATOS_BAQ ="Los datos de las Ot's se cargarón correctamente.";
	
	
	private const string CAMPO_TABLAUD_COMPANY = "Company";
	private const string CAMPO_TABLAUD_KEY1 = "Key1";
	private const string CAMPO_TABLAUD_KEY2 = "Key2";
	private const string CAMPO_TABLAUD_KEY3 = "Key3";
	private const string CAMPO_TABLAUD_KEY4 = "Key4";
	private const string CAMPO_TABLAUD_KEY5 = "Key5";
	private const string CAMPO_OT = "Ot";

	
	private const string CAMPO_IMPRESION_ROTULO = "IMPRESION ROTULO";
	private const string RUTA_GUARDADO_ARCHIVO = @"\\200.100.9.18\\TransferenciaBobinas\\";
	
	#endregion

	#region MÉTODOS: CONSTRUCTOR Y DESTRUCTOR DE LA CLASE
	
	public void InitializeCustomCode()
	{
		// ** Wizard Insert Location - Do not delete 'Begin/End Wizard Added Variable Initialization' lines **
		// Begin Wizard Added Variable Initialization

		// End Wizard Added Variable Initialization

		// Begin Wizard Added Custom Method Calls report
		
		this.ebtnConsultar.Click += new System.EventHandler(this.ebtnConsultar_Click);
		this.btnImprimir.Click += new System.EventHandler(this.Imprimir_Click);
		this.eurgdOts.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.eurgdOts_KeyPress);
   	 // End Wizard Added Custom Method Calls//Create a table that will contain three columns.
		//this.eurgdOts.DisplayLayout.Bands[0].Columns.Add("Ot");
		
		DataTable dt = new DataTable();
		dt.Clear();
		dt.Columns.Add(CAMPO_OT);
		DataRow lColumnaNueva = dt.NewRow();
		lColumnaNueva[CAMPO_OT] = "";
		dt.Rows.Add(lColumnaNueva);
		
		this.eurgdOts.DataSource= dt;
		this.eurgdOts.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
		this.mCurrentCompany = ((Ice.Core.Session)oTrans.Session).CompanyID;
	}

	public void DestroyCustomCode()
	{
		// ** Wizard Insert Location - Do not delete 'Begin/End Wizard Added Object Disposal' lines **
		// Begin Wizard Added Object Disposal

		this.ebtnConsultar.Click -= new System.EventHandler(this.ebtnConsultar_Click);
		this.btnImprimir.Click -= new System.EventHandler(this.Imprimir_Click);
		this.eurgdOts.KeyPress -= new System.Windows.Forms.KeyPressEventHandler(this.eurgdOts_KeyPress);
		// End Wizard Added Object Disposal

		// Begin Custom Code Disposal

		// End Custom Code Disposal
	}

	#endregion

	#region EVENTOS DESDE EpiViewNotification

	#endregion

	#region EVENTOS GENERADOS POR EL FORMULARIO
	
	private void UD09Form_Load(object sender, EventArgs args)
	{
		// Add Event Handler Code
		configurarPantallaCustomizada();
		
		//this.UD09Form.WindowState = FormWindowState.Maximized;
		MenusDeEpicor lMenus = new MenusDeEpicor(this.oTrans);
		lMenus.RegistrarIngresoAMenu();

		this.mUsuario = ((Ice.Core.Session)oTrans.Session).UserID;

	}
	private void ebtnConsultar_Click(object sender, System.EventArgs args)
	{
		ConsultarInformacionOTSaImprimir();
		// ** Place Event Handling Code Here **
	}

	private void Imprimir_Click(object sender, System.EventArgs args)
	{
		
		ValidarValoresUD09();
		// ** Place Event Handling Code Here **
	}


	private void eurgdOts_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
	{
		if (e.KeyChar == (char)13 || e.KeyChar == (char)9 )
        {
			DataTable dt =(DataTable)eurgdOts.DataSource;
			
				DataRow _ravi = dt.NewRow();
				_ravi["Ot"] = "";
				dt.Rows.Add(_ravi);

				this.eurgdOts.DataSource= dt;
			
			e.Handled = true;
    //        Infragistics.Win.UltraWinGrid.UltraGridCell cell =  eurgdOts.Rows[0].Cells[0];
    //        eurgdOts.CurrentCell = cell;
    //        eurgdOts.BeginEdit(true);               
        }
		// ** Place Event Handling Code Here **
	}
	#endregion

	#region MÉTODOS Y FUNCIONES PRIVADAS

	public void ConsultarInformacionOTSaImprimir()
	{
			try
			{
			//  Dictionary&lt;string,string&gt; lParametros = null;
				this.epiUltraGridC1.DataSource = null;
				List&lt;string&gt; lParametros = new List&lt;string&gt;();
				DataTable lDataTable = null;
				//lParametros = new Dictionary&lt;string,string&gt;();				
				DataTable ldt = (DataTable)eurgdOts.DataSource;
				foreach (DataRow dr in ldt.Rows)
				{
					lParametros.Add(CAMPO_LISTAOT+"|"+dr["Ot"]);
				}				
				lDataTable = BD.ConsultaBAQList(NOMBRE_BAQ , oTrans, lParametros);
				System.Data.DataColumn lcantImpresion = new System.Data.DataColumn("Cant. Impresiones", typeof(int));
				lcantImpresion.DefaultValue = 1;
				lDataTable.Columns.Add(lcantImpresion);
				 
				if(lDataTable.Rows.Count &gt; 0)
				{

					this.epiUltraGridC1.DataSource = lDataTable;
									
					MessageBox.Show(MENSAJE_CARGO_DATOS_BAQ);
				}
				else
				{
					MessageBox.Show(MENSAJE_ERROR_DATOS_BAQ);
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
	
			
	}	

	private void ValidarValoresUD09()
	{
		DataTable lDataTable = (DataTable) this.epiUltraGridC1.DataSource;

		if (lDataTable.Rows.Count &gt; 0)
		{
				
			foreach (DataRow td in lDataTable.Rows)
			{
				
				if ( td[6].ToString().Equals("S"))
				{
					DialogResult lVentanaDeDialogo = MessageBox.Show("Algunos de los registros seleccionados ya han sido impresos en anteriores ocasiones, ¿Desea reimprimirlos?", "Conflicto", MessageBoxButtons.YesNo);
					if(lVentanaDeDialogo == DialogResult.Yes)
					{
						BorrarValoresUD09(td,0);

						for (int i = 0; i &lt; (int) td[8]; i++){
							GuardarValoresUD09(td, i);
						}
						if((int)td[8] &gt; 1 ){      					// Se eliminan las copias
							for(int i = 1; i &lt; (int)td[8]; i++){
								BorrarValoresUD09(td,i);
							}
						}
					}
				}
				else
				{
					
					for (int i = 0; i &lt; (int) td[8] ; i++){
						
						    GuardarValoresUD09(td, i);

						}
						if((int)td[8] &gt; 1 ){      					// Se eliminan las copias
							for(int i = 1; i &lt; (int)td[8]; i++){
								BorrarValoresUD09(td,i);
							}
						}
				}							
				
						
			}
			MessageBox.Show("Imprimiendo archivos");

		}
		else 
		{
			MessageBox.Show("La grilla debe tener al menos un campo ");
		}
	}
	
	private void GuardarValoresUD09(DataRow pDataRow_Impresiones, int nCopias)
	{
		bool lGuardo= false;
		bool lConecto = false;
		
		 UD09Adapter lUD09 = new  UD09Adapter(oTrans);
		lConecto = lUD09.BOConnect();
		if (lConecto)
		{
			
				lUD09.GetaNewUD09();
				lUD09.UD09Data.UD09.Rows[0].BeginEdit();
		
				lUD09.UD09Data.UD09.Rows[0][CAMPO_TABLAUD_COMPANY] = mCurrentCompany;

				lUD09.UD09Data.UD09.Rows[0][CAMPO_TABLAUD_KEY1] = CAMPO_IMPRESION_ROTULO;
				lUD09.UD09Data.UD09.Rows[0][CAMPO_TABLAUD_KEY2] = pDataRow_Impresiones[0].ToString(); // OT
				lUD09.UD09Data.UD09.Rows[0][CAMPO_TABLAUD_KEY3] = pDataRow_Impresiones[1].ToString(); //KVA
				lUD09.UD09Data.UD09.Rows[0][CAMPO_TABLAUD_KEY4] = pDataRow_Impresiones[2].ToString(); //BIL
				lUD09.UD09Data.UD09.Rows[0][CAMPO_TABLAUD_KEY5] = nCopias.ToString();  				  //Identificador de numero de copias

				lUD09.UD09Data.UD09.Rows[0]["CHARACTER01"] =  pDataRow_Impresiones[3].ToString(); 	  //Voltaje primario
				lUD09.UD09Data.UD09.Rows[0]["CHARACTER02"] =  pDataRow_Impresiones[4].ToString();  	  //Voltaje secundario
				lUD09.UD09Data.UD09.Rows[0]["CHARACTER03"] = "S";
				lUD09.UD09Data.UD09.Rows[0]["ShortChar01"] = mUsuario;
				lUD09.UD09Data.UD09.Rows[0]["DATE01"] = DateTime.Now;

				lUD09.UD09Data.UD09.Rows[0].EndEdit();
			
			lUD09.Update();	
			lGuardo = true;
			string lOt = pDataRow_Impresiones[0].ToString();
			string lKVA = pDataRow_Impresiones[1].ToString().Replace(",",".");
			string lBIL = pDataRow_Impresiones[2].ToString();
			string lVP = pDataRow_Impresiones[3].ToString();
			string lVS = pDataRow_Impresiones[4].ToString();
			string lDiseno = pDataRow_Impresiones[5].ToString();
			string lFechaHornoOriginal = pDataRow_Impresiones[7].ToString();
				
			string fecha = DateTime.Now.ToString("yyyy-MM-dd");
			string hora = DateTime.Now.ToString("HH:mm:ss");
			hora = hora.Replace(":", "-");

			string lCadena = lOt + "," + lKVA + "," + lDiseno + "," + lVP + "," + lVS + "," + lBIL + "," + Convert.ToDateTime(lFechaHornoOriginal).ToString("dd-MM-yyyy");
			
			string lNombre = "RotuloBobinas" + lOt + "_" + lDiseno + "_" + fecha + hora + "_" + nCopias + ".dat";
			GuardarArchivo(lCadena, lNombre);	
			
					
		}
	}

	private void BorrarValoresUD09(DataRow pDataRow_Impresiones, int nCopias)
	{
		bool lGuardo= false;
		bool lConecto = false;
	    UD09Adapter lUD09 = new  UD09Adapter(oTrans);
		
		bool lResultado = false;
		
		lConecto = lUD09.BOConnect();
		
		SearchOptions lCondicion =  new SearchOptions(SearchMode.AutoSearch);
		
		lCondicion.NamedSearch.WhereClauses.Add("UD09", "Company = '" + ((Ice.Core.Session)oTrans.Session).CompanyID + "' and Key2 ='" + pDataRow_Impresiones[0].ToString() +"'");
		
		DataSet lDatos = ((UD09Adapter)oTrans.AdapterList[0]).GetRows(lCondicion, out lResultado);
		
		DataTable lDatosUD09 = lDatos.Tables["UD09"];

  	  object[] lParametros = new object[5];
        
        if (lConecto)
		{
			lParametros[0] = CAMPO_IMPRESION_ROTULO;
	        //lParametros[1] = pDataRow_Impresiones[0].ToString();
	        //lParametros[2] = pDataRow_Impresiones[1].ToString();
	        //lParametros[3] = pDataRow_Impresiones[2].ToString();
	        //lParametros[4] = nCopias.ToString();
			
			lParametros[1] = lDatosUD09.Rows[0]["Key2"].ToString();
	        lParametros[2] = lDatosUD09.Rows[0]["Key3"].ToString();
	        lParametros[3] = lDatosUD09.Rows[0]["Key4"].ToString();
	        lParametros[4] = nCopias.ToString();
	
			lUD09.DeleteByID(lParametros);
			lUD09.Update();	
			lGuardo = true;
	
					
		}else{
			MessageBox.Show("No existe conexión");		
		}
	}

	private void GuardarArchivo (string lCadena, string pNombre)
	{
		try{
			
			
			StreamWriter sw = new StreamWriter(RUTA_GUARDADO_ARCHIVO+ pNombre);
			
			//Write a line of text
			sw.WriteLine("ot,kva,diseño,vp,vs,bil,horno");
			
			//Write a second line of text
			sw.WriteLine(lCadena);
			
			//Close the file
			sw.Close();
			
		}
		catch(Exception e)
		{
		MessageBox.Show("Exception: " + e.Message);
		}
		finally 
		{
	}
	}
	#endregion

	#region MÉTODOS Y FUNCIONES VISUALES UD09
	

	private void configurarPantallaCustomizada()
	{
		ocultarElementosPantalla("windowDockingArea1",false);
		ocultarElementosPantalla("groupBox1",false);
		ocultarElementosPantalla("btnKeyField",false);
		ocultarElementosPantalla("grdList",false);	
		ocultarElementosPantalla("listPanel1",false);
		
		definirTamañoDePantalla(1350, 750);//(1004, 822);
		centrarPantalla();
	
		UD09Form.MaximizeBox = true;
		ocultarIconosDeLaBarraDeMenus();
		egrbxTodo.Dock = DockStyle.Fill;

		VentanaCustomizada lVentana = new VentanaCustomizada(this.UD09Form, this.baseToolbarsManager);
			ConfiguracionDeVentana lConfiguracion = new ConfiguracionDeVentana(lVentana);
			lConfiguracion.iniciarPantallaMaximizada(true);
			lConfiguracion.ocultarBarraCompletaDeMenus(true);	
			lConfiguracion.configurarPantalla();
	}

	
	private void centrarPantalla()
	{
		UD09Form.Left = (Screen.PrimaryScreen.WorkingArea.Width - UD09Form.Width) / 2;
		UD09Form.Top = (Screen.PrimaryScreen.WorkingArea.Height - UD09Form.Height)/ 2;
	}	

	private void definirTamañoDePantalla(int pAncho, int pAlto)
	{
		UD09Form.Width = pAncho;
		UD09Form.Height = pAlto;
	}

	private void ocultarIconosDeLaBarraDeMenus()
	{
		try
		{
			this.baseToolbarsManager.Tools["NewTool"].SharedProps.Visible = false;
			this.baseToolbarsManager.Tools["SaveTool"].SharedProps.Visible = false;
			this.baseToolbarsManager.Tools["DeleteTool"].SharedProps.Visible = false;
		}
		catch(Exception ex)
		{
			MessageBox.Show(ex.Message);
		}
	}

	private void ocultarElementosPantalla(string pNombreElemento, bool pMostrar)
	{
		Control[]  lControles = UD09Form.Controls.Find(pNombreElemento, true);
		lControles[0].Visible = pMostrar;
	}

	private void searchOnCOASegValuesAdapterShowDialog()
	{
		bool lRecSelected;
		string lWhereClause = "COACode = 'NIIF-ZF' AND Type = 'B'";
		System.Data.DataSet lDsCOAActCatAdapter = Ice.UI.FormFunctions.SearchFunctions.listLookup(this.oTrans, "COAActCat", out lRecSelected, true, lWhereClause);
		
		if (lRecSelected)
		{
			System.Data.DataRow lAdapterRow = lDsCOAActCatAdapter.Tables[0].Rows[0];
	//		etxtCuentaMayor.Text = lAdapterRow["CategoryID"].ToString();
	//		etxtCuentaDesc.Text = lAdapterRow["Description"].ToString();
		}
	}  

	private DataTable crearDataset()
	{
		DataTable dt = new DataTable();
		dt.Rows.Add("Jan 1", 35, 10, 40, 30, 45);
		return dt;
	}
	#endregion


}