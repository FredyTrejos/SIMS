<?php
error_reporting(E_ALL && ~E_NOTICE);
session_start();
class frontEnd{

    function __construct(){

    }

    function __destruct(){
        
    }

//--------------------------------------------------

    public function encabezado($ruta){

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIMS</title>
    <link rel="stylesheet" href="<?php echo $ruta ?>scripts/style.css">
    <link rel="icon" href="recursos/logo1.png">

</head>

<body>
    <center>
        <div id="fondo">
            <header>
                <div id="titulo">
                    <h1 id="tit1">SIMS</h1>
                    <h2 id="tit2">Sistema de Información para Mesa de Servicio</h2>
                    <h3 id="tit3">Centro de Diseño e Innovación Técnologica Industrial</h3>
                </div>
                <div id="logo"></div>
            </header>
<?php
}

//--------------------------------------------

    public function menuPpal(){//menú principal
?>
    <nav>
        <div class="botonins" onclick="window.location='./index.php?opc=1';">Inicio</div>
        <div class="botonins" onclick="window.location='./index.php?opc=2';">Institucional</div>
    </nav>
    <section>
<?php
    }

//--------------------------------------------

    public function menuAdmin(){
?>     
    <nav>
        <div class="botonins" onclick="window.location='./administrador.php?opc=11';">Usuarios</div>
        <div class="botonins" onclick="window.location='./administrador.php?opc=21';">Equipos</div>
        <div class="botonins" onclick="window.location='./administrador.php?opc=31';">Mantenimientos</div>
        <div class="botonins" onclick="window.location='./administrador.php?opc=41';">Perfil</div>
        <div class="botonins" onclick="window.location='./administrador.php?opc=99';">Cerrar sesión</div>
    </nav>
    <section>
<?php 
    }

//--------------------------------------------

    public function menuUser(){
?>     
    <nav>
        <div class="botonins" onclick="window.location='./tecnico.php?opc=21';">Equipos</div>
        <div class="botonins" onclick="window.location='./tecnico.php?opc=41';">Perfil</div>
        <div class="botonins" onclick="window.location='./tecnico.php?opc=99';">Cerrar sesión</div>
    </nav>
    <section>
<?php 
        }

//--------------------------------------------

    public function piePagina(){
?>
    </section>
                <footer>
                    <p id="tAbajo">Copyright @ 2023 - Todos los derechos Reservados. <a href="https://www.sena.edu.co/es-co/Paginas/politicasCondicionesUso.aspx" target="_blank">Política de Privacidad y
                            Términos de Uso. </a><br>
                        Diseño y Maquetación Jhon Fredy Trejos Morales e-mail: jhoftrejos@soy.sena.edu.co</p>
                </footer>
            </div>
        </center>
    </body>

    </html>
<?php
    }

//-----------------------------------------------------------

    public function institucional(){
?>
    <article id="artInstitucional">
        <div class="divInstitucional">
            <p class="titMisionvision">Misión</p>
            <p class="textInstitucional">El Servicio Nacional de Apredizaje (SENA) se encarga de cumplir la función que le corresponde al
                Estado de invertir en el desarrollo social y técnico de los trabajadores colombianos, ofreciendo y 
                ejecutando la Formación Profesional Integral gratuita, para la incorporación y el desarrollo de las
                personas en actividades productivas que contribuyan al desarrollo social, económico y tecnológico del
                país.</p>
        </div>
        <div class="divInstitucional">
            <p class="titMisionvision">Visión</p>
            <p class="textInstitucional">El SENA será una organización de conocimiento para todos los colombianos, innovando
                permanentemente en sus estrategias y metodologías de apredizaje, en total acuerdo con las 
                tendencias y cambios tecnológicos y las necesidades del sector empresarial y de los trabajadores,
                impactando positivamente la productividad, la competitividad, la equidad y el desarrollo del país.
            </p>
        </div>
    </article>
<?php   
}

//----------------------------------------------------

public function loginF(){
?>
    <aside>
        <h4 id="tIngreso">Ingresar</h4>
        <form action="" method="POST">
            <table>
                <tr>
                    <td align="right" height="30"><p><strong>Usuario</strong></p></td>
                    <td>
                        <input class="campo" type="text" name="usu" value="" autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <td align="right" height="30"><p><strong>Contraseña</strong></p></td>
                    <td>
                        <input class="campo" type="password" name="cont" value="" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <input class="logear" type="submit" name="okey" value="LogIn">
                    </td>
                </tr>
            </table>
        </form>
    </aside>
    <article id="articleindex">
    </article>
<?php
$this->login();
}
  
//--------------------------------------------------------------------

	private function conectaDB() {
		$dbase = 'sims';
		$user = 'rootsims';
		$pass = '12345';
		$dsn = "mysql:dbname=".$dbase.";host=localhost";
		
		try {
			$link = new PDO( $dsn, $user, $pass );
			return $link;
		}
		catch ( PDOException $e ) {
			echo "Error de conexion a DB" . $e->getMessage();
		}
	}
//----------------------------------------------------------
	private function login() {
		if( isset( $_POST['usu'] )  && isset( $_POST['cont'] ) ) {
			$usuario = $_POST['usu'];
			$clave = $_POST['cont'];
			
            $clave=SHA1 (MD5($clave));

			$SQL = "SELECT tipo,contrasena FROM usuarios WHERE id_usuario = ? ;";
			
			$conn = $this->conectaDB();
			
			try {
				$stm = $conn->prepare( $SQL );
				$stm->bindValue(1, $usuario);
				
				$ok = $stm->execute();
				
				$rs = $stm->fetch(PDO::FETCH_ASSOC);
				if( $rs>0 ) {	
					if( $rs['contrasena'] == $clave ) {	

                        $_SESSION["id"]=$usuario;	

						switch( $rs['tipo'] ) {
							case 'A':
                                    $_SESSION['ctrlogA']="@itwharl4u28isa?_fifrvr";
									echo "<script>window.location='./interfaces/administrador.php?opc=11';</script>";
									break;
										
							case 'T':
                                    $_SESSION['ctrlogT']= "@mifjdeiofjefij_28lkl*mn";
									echo "<script>window.location='./interfaces/tecnico.php?opc=21';</script>";
						}
					}
					else {
						echo "<script>window.alert('Error combinación Usuario/Clave !!!');</script>";
					}
				}
				else {
					echo "<script>window.alert('Error combinación Usuario/Clave !!!');</script>";
				}		
			}
			catch ( PDOException $e ) {
				echo "Error de consulta" . $e->getMessage();
			}
			
		}
	}

//----------------------------Función actualizar perfil----------------------------------//

    public function actualizarPerfil($id_usuario){
        $sSQL = "SELECT * FROM usuarios WHERE id_usuario=?;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id_usuario); 

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);
?>

<script>
	function confirmar(formula){
		var cont1 = document.getElementById("cont1").value;
		var cont2 = document.getElementById("cont2").value;


		
		if(cont1===cont2){
            formula.submit();     
        } 
        else{
            window.alert("¡Las contraseñas no coinciden!\nVerifique su nueva contraseña.");
        }
	}
	
	</script>
<article id="artForm">
    <h4 id="tForm">Actualizar Datos Personales</h4>
    <form action="" method="POST">
        <table>
            <tr>
                <td align="right"><p><strong>Contraseña Actual</strong></p></td>
                <td colspan="1"><input type="password" name="contrasena" value="" class="entrada">
                <td colspan="1"><input type="hidden" name="id_usuario" value="<?php echo $rs["id_usuario"];?>" class="entrada"></td>
                <td colspan="1"><input type="hidden" name="tipo" value="<?php echo $rs["tipo"];?>" class="entrada"></td>

                    </td>
            </tr>
            <tr>
                <td align="right"><p><strong>Contraseña Nueva</strong></p></td>
                <td colspan="1"><input type="password" name="contrasena1" value="" class="entrada" id="cont1"></td>
            </tr>
            <tr>
                <td align="right"><p><strong>Verificar Contraseña</strong></p></td>
                <td colspan="1"><input type="password" name="contrasena2" value="" class="entrada" id="cont2"></td>
            </tr>
            <tr>
                <td align="right"><p><strong>Dirección</strong></p></td>
                <td colspan="3"><input type="text" class="entrada" name="direccion" value="<?php echo $rs["direccion"];?>" /></td>
            </tr>
            <tr>
                <td align="right"><p><strong>Teléfono</strong></p></td>
                <td><input type="text" class="entrada" name="telefono" value="<?php echo $rs['telefono'];?>" /></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td align="right"><p><strong>Empresa</strong></p></td>
                <td colspan="3"><input type="text" name="empresa" value="<?php echo $rs['empresa']; ?>" class="entrada"></td>

            </tr>
            <tr>
                <td align="right"><p><strong>E-mail</strong></p></td>
                <td colspan="3"><input type="text" name="email" value="<?php echo $rs['email']; ?>" class="entrada"></td>

            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><input type="reset" name="eliminar" value="Cancelar" class="enviarBoton" onclick="window.location='./<?php if($rs["tipo"]=="A") { echo "administrador.php?opc=11"; } else { echo "tecnico.php?opc=21"; }?>';"></td>
                <td><input type="button" name="enviarf" value="Enviar" class="enviarBoton" onclick="confirmar(this.form);"></td>    
            </tr>
        </table>
    </form>
</article>

<?php
        }
        catch ( PDOException $e ) {
            echo "Error de consulta" . $e->getMessage();
        }
        $this->verificarActual();
    }

//----------------------------------------------------

    private function verificarActual(){
        if(isset($_POST['id_usuario'])){

            $contrasena     = $_POST['contrasena'];
            $id_usuario      = $_POST['id_usuario'];
            $tipo      = $_POST['tipo'];


            $contrasena=SHA1(MD5($contrasena));

			$SQL = "SELECT contrasena, tipo FROM usuarios WHERE id_usuario=? ;";

			$conn = $this->conectaDB();

            try {
				$stm = $conn->prepare( $SQL );
				$stm->bindValue(1, $id_usuario);

				
				$ok = $stm->execute();
				$rs = $stm->fetch(PDO::FETCH_ASSOC);

                if($rs['contrasena'] != $contrasena){
                    echo "<script>window.alert('¡¡¡La contraseña actual no coincide!!!');</script>";
                    
                    if($rs["tipo"] == "A"){
                        echo "<script>window.location='./administrador.php?opc=11';</script>";
                    }
                    else if($rs["tipo"] == "T"){
                         echo "<script>window.location='./tecnico.php?opc=21';</script>";
                    }
                }
                
                $contrasena2  = $_POST['contrasena2'];
                $direccion    = $_POST['direccion'];
                $telefono     = $_POST['telefono'];
                $empresa      = $_POST['empresa'];
                $email        = $_POST['email'];
                $id_usuario   = $_POST['id_usuario'];
        
                $contrasena2=SHA1(MD5($contrasena2));
        
                $sSQL = "UPDATE usuarios SET contrasena=?,direccion=?,telefono=?,empresa=?,email=? ";
                $sSQL .= "WHERE id_usuario=?;";
        
                $conn = $this->conectaDB();
                $stm = $conn->prepare( $sSQL );
            
                $stm->bindValue(1, $contrasena2);
                $stm->bindValue(2, $direccion);
                $stm->bindValue(3, $telefono);
                $stm->bindValue(4, $empresa);
                $stm->bindValue(5, $email);
                $stm->bindValue(6, $id_usuario);
                
                $ok = $stm->execute();

                if($rs["tipo"] == "A"){
                    echo "<script>window.location='./administrador.php?opc=11';</script>";
                }
                else if($rs["tipo"] == "T"){
                    echo "<script>window.location='./tecnico.php?opc=21';</script>";
                }


            }
            catch ( PDOException $e ) {
                echo "Error de consulta" . $e->getMessage();
            }    
        }  
    }

//----------------------------------------------------

}
?>





