<?php
class usuarios{
    function __construct(){

    }

    function __destruct(){

    }

//------------------------------------------------------

    private function conectaDB() {
		$dbase = 'sims';
		$user = 'rootsims';
		$pass = '12345';
		$dsn = "mysql:dbname=".$dbase.";host=localhost";
		
		try {
			$link = new PDO( $dsn, $user, $pass );
			return $link;
		}
		catch ( PDOException $e ) {
			echo "Error de conexion a DB" . $e->getMessage();
		}
	}

//------------------------------------------------------

    public function listado(){
?>
    <article id="listadoAr" >
        <h4 id="titListado">Listado de Usuarios</h4>
        <table border="1" id="tablaLis">
                <tr>
                    <th colspan="3" width="1">
                        <input class="registroN" name="nuevoReg" type="button" value="Nuevo Usuario" onclick="window.location='./administrador.php?opc=12';"/>
                    </th>
                    <th>Identificación</th>
                    <th>Nombre</th>
                    <th>Nickname</th>
                    <th>Empresa</th>
                    <th>Telefono</th>
                    <th>Tipo</th>
                </tr>
<?php
        $sSQL = "SELECT * FROM usuarios;";

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            //$stm->bindValue(1, $var); si hubiera un filtrado

            $ok = $stm->execute();
            $fila=0;
            while($rs = $stm->fetch(PDO::FETCH_ASSOC)){
                if(($fila%2)==0) {echo "<tr>";}
                else{echo "<tr class='par'>";}
                
?>
                <td height="20" align="center"><img src="../recursos/b_edit.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=14&id=<?php echo $rs['id_usuario']; ?>';" /></td>
                <td align="center"><img src="../recursos/b_drop.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=15&id=<?php echo $rs['id_usuario']; ?>';" /></td>
                <td align="center"><img src="../recursos/b_search.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=13&id=<?php echo $rs['id_usuario']; ?>';" /></td>
                <td><?php echo $rs["id_usuario"];?></td>
                <td width="220"><?php echo $rs["nombres"]." ".$rs["apellidos"];?></td>
                <td><?php echo $rs["nickname"];?></td>
                <td><?php echo $rs["empresa"];?></td>
                <td><?php echo $rs["telefono"];?></td>
                <td align="center" width="30"><?php echo $rs["tipo"];?></td>
<?php
                 echo "</tr>";
                 $fila++;
            }


        }
        catch ( PDOException $e ) {
            echo "Error de consulta" . $e->getMessage();
        }

?>

            
        </table>
    </article>
<?php



    }

//------------------------------------------------------

    public function fagregar(){
?>
                <article id="artForm">
                    <h4 id="tForm">Registrar Usuarios</h4>
                    <form action="" method="POST">
                        <table>
                            <tr>
                                <td align="right"><p><strong>Identificación</strong></p></td>
                                <td><input size="4" type="text" name="id_usuario" value="" class="entrada"></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Apellidos</strong></p></td>
                                <td colspan="2"><input type="text" class="entrada" name="apellidos" value=""></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Nombres</strong></p></td>
                                <td colspan="2"><input type="text" class="entrada" name="nombres" value=""/></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Nickname</strong></p></td>
                                <td><input type="text" class="entrada" name="nickname" value=""/></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Contraseña</strong></p></td>
                                <td colspan="3"><input type="password" name="contrasena" value="" class="entrada"></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Dirección</strong></p></td>
                                <td><input type="text" class="entrada" name="direccion" value="" /></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Teléfono</strong></p></td>
                                <td><input type="text" class="entrada" name="telefono" value=""></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Empresa</strong></p></td>
                                <td colspan="3"><input type="text" name="empresa" value="" class="entrada"></td>

                            </tr>
                            <tr>
                                <td align="right"><p><strong>E-mail</strong></p></td>
                                <td colspan="3"><input type="text" name="email" value="" class="entrada"></td>

                            </tr>
                            <tr>
                                <td align="right"><p><strong>Tipo de Usuario</strong></p></td>
                                <td align="center"><input type="radio" name="tipo" value="A" class="rad"/>Administrador</td>
                                <td><input type="radio" name="tipo" value="T" class="rad" checked/>Técnico</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./administrador.php?opc=11';"/></td>
                                <td><input type="submit" name="enviarf" value="Registrar" class="enviarBoton"/></td>
                            </tr>
                        </table>
                    </form>
                </article>
<?php
        $this->agregar();
    }

//------------------------------------------------------

    private function agregar(){

        if(isset($_POST['enviarf'])){
            $id_usuario     = $_POST['id_usuario'];
            $apellidos      = $_POST['apellidos'];
            $nombres        = $_POST['nombres'];
            $nickname       = $_POST['nickname'];
            $contrasena     = $_POST['contrasena'];
            $direccion      = $_POST['direccion'];
            $telefono       = $_POST['telefono'];
            $empresa        = $_POST['empresa'];
            $email          = $_POST['email'];
            $tipo           = $_POST['tipo'];

            $contrasena=SHA1 (MD5($contrasena));


            $sSQL = "INSERT INTO usuarios VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            $conn = $this->conectaDB();

            try{
                $stm = $conn->prepare( $sSQL );
                $stm->bindValue(1, $id_usuario);
                $stm->bindValue(2, $tipo);
                $stm->bindValue(3, $apellidos);
                $stm->bindValue(4, $nombres);
                $stm->bindValue(5, $nickname);
                $stm->bindValue(6, $contrasena);
                $stm->bindValue(7, $direccion);
                $stm->bindValue(8, $telefono);
                $stm->bindValue(9, $empresa);
                $stm->bindValue(10, $email);
                

                $ok = $stm->execute();

                echo "<script>window.location='./administrador.php?opc=11';</script>";

            }
            catch ( PDOException $e ) {
                echo "Error de consulta" . $e->getMessage();
            }
        }


    }


    public function fmodificar($id_usuario){
        $sSQL = "SELECT * FROM usuarios WHERE id_usuario=?;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id_usuario); 

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);
    ?>
	
	
        <article id="artForm">
            <h4 id="tForm">Modificar Usuarios</h4>
            <form action="" method="POST">
                <table>
                    <tr>
                        <td align="right"><p><strong>Identificación</strong></p></td>
                        <td><input type="text" name="id_usuario" disabled value="<?php echo $rs["id_usuario"];?>" class="entrada">
							<input type="hidden" name="id_oculto" value="<?php echo $rs["id_usuario"];?>" class="entrada">
						</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Apellidos</strong></p></td>
                        <td colspan="2"><input type="text" class="entrada" name="apellidos" value="<?php echo $rs["apellidos"];?>"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Nombres</strong></p></td>
                        <td colspan="2"><input type="text" class="entrada" name="nombres" value="<?php echo $rs["nombres"];?>"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Nickname</strong></p></td>
                        <td><input type="text" class="entrada" name="nickname" value="<?php echo $rs["nickname"];?>"/></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Contraseña</strong></p></td>
                        <td colspan="3"><input type="password" name="contrasena" value="<?php echo $rs["contrasena"];?>" class="entrada"></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Dirección</strong></p></td>
                        <td><input type="text" class="entrada" name="direccion" value="<?php echo $rs["direccion"];?>" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Teléfono</strong></p></td>
                        <td><input type="text" class="entrada" name="telefono" value="<?php echo $rs["telefono"];?>"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Empresa</strong></p></td>
                        <td colspan="3"><input type="text" name="empresa" value="<?php echo $rs["empresa"];?>" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>E-mail</strong></p></td>
                        <td colspan="3"><input type="text" name="email" value="<?php echo $rs["email"];?>" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Tipo de Usuario</strong></p></td>
                        <td align="center"><input type="radio" name="tipo" value="A" class="rad" <?php if($rs["tipo"]=="A") {echo "checked";}?> />Administrador</td>
                        <td><input type="radio" name="tipo" value="T" class="rad" <?php if($rs["tipo"]=="T") {echo "checked";}?> />Técnico</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
						<td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./administrador.php?opc=11';"/></td>
                        <td><input type="submit" value="Modificar" class="enviarBoton" name="enviarf"/></td>
						
                    </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
	$this->modificar();
    }

//------------------------------------------------------

	private function modificar(){
		       if(isset($_POST['enviarf'])){
            $id_usuario     = $_POST['id_oculto'];
            $apellidos      = $_POST['apellidos'];
            $nombres        = $_POST['nombres'];
            $nickname       = $_POST['nickname'];
            $contrasena     = $_POST['contrasena'];
            $direccion      = $_POST['direccion'];
            $telefono       = $_POST['telefono'];
            $empresa        = $_POST['empresa'];
            $email          = $_POST['email'];
            $tipo           = $_POST['tipo'];

            $contrasena=SHA1 (MD5($contrasena));
            
            $sSQL = "UPDATE usuarios SET apellidos=?,nombres=?,nickname=?,contrasena=?,direccion=?,telefono=?,empresa=?,email=?,tipo=? ";
			$sSQL .= "WHERE id_usuario=? LIMIT 1;";

            $conn = $this->conectaDB();

            try{
                $stm = $conn->prepare( $sSQL );
                
                $stm->bindValue(1, $apellidos);
                $stm->bindValue(2, $nombres);
                $stm->bindValue(3, $nickname);
                $stm->bindValue(4, $contrasena);
                $stm->bindValue(5, $direccion);
                $stm->bindValue(6, $telefono);
                $stm->bindValue(7, $empresa);
                $stm->bindValue(8, $email);
                $stm->bindValue(9, $tipo);
				$stm->bindValue(10, $id_usuario);
                

                $ok = $stm->execute();

                echo "<script>window.location='./administrador.php?opc=11';</script>";

            }
            catch ( PDOException $e ) {
                echo "Error de consulta" . $e->getMessage();
            }
        }
	}

//------------------------------------------------------

    public function consultar($id_usuario){

        $sSQL = "SELECT * FROM usuarios WHERE id_usuario=?;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id_usuario); 

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);


    ?>
        <article id="artForm">
            <h4 id="tForm">Consultar Usuarios</h4>
            <form action="" method="POST">
                <table>
                    <tr>
                        <td align="right"><p><strong>Identificación</strong></p></td>
                        <td><input type="text" name="id_usuario" disabled value="<?php echo $rs["id_usuario"];?>" class="entrada"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Apellidos</strong></p></td>
                        <td colspan="2"><input type="text" class="entrada" name="apellidos" disabled value="<?php echo $rs["apellidos"];?>"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Nombres</strong></p></td>
                        <td colspan="2"><input type="text" class="entrada" name="nombres" disabled value="<?php echo $rs["nombres"];?>"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Nickname</strong></p></td>
                        <td><input type="text" class="entrada" name="nickname" disabled value="<?php echo $rs["nickname"];?>"/></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Contraseña</strong></p></td>
                        <td colspan="3"><input type="password" name="contrasena" disabled value="<?php echo $rs["contrasena"];?>" class="entrada"></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Dirección</strong></p></td>
                        <td><input type="text" class="entrada" name="direccion" disabled value="<?php echo $rs["direccion"];?>" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Teléfono</strong></p></td>
                        <td><input type="text" class="entrada" name="telefono" disabled value="<?php echo $rs["telefono"];?>"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Empresa</strong></p></td>
                        <td colspan="3"><input type="text" name="empresa" disabled value="<?php echo $rs["empresa"];?>" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>E-mail</strong></p></td>
                        <td colspan="3"><input type="text" name="email" disabled value="<?php echo $rs["email"];?>" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Tipo de Usuario</strong></p></td>
                        <td align="center"><input type="text" name="tipo" disabled value="<?php echo $rs["tipo"];?>" class="entrada" size="1"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><input type="button" value="Regresar" class="enviarBoton" onclick="window.location='./administrador.php?opc=11';"/></td>
                    </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
    }

//------------------------------------------------------

    public function feliminar($id_usuario){
	$sSQL = "SELECT * FROM usuarios WHERE id_usuario=?;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id_usuario); 

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);


    ?>
	
	<script>
	function confirmar(formu){
		var res = window.confirm("¿Realmente desea eliminarlo?");
		
		if(res){
			formu.submit();
		} 
		else{
			window.location='./administrador.php?opc=11';
		}
	}
	
	</script>
        <article id="artForm">
            <h4 id="tForm">Eliminar Usuarios</h4>
            <form action="" method="POST">
                <table>
                    <tr>
                        <td align="right"><p><strong>Identificación</strong></p></td>
                        <td><input type="text" name="id_usuario" disabled value="<?php echo $rs["id_usuario"];?>" class="entrada">
						<input type="hidden" name="id_oculto" value="<?php echo $rs["id_usuario"];?>" class="entrada">
						</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Apellidos</strong></p></td>
                        <td colspan="2"><input type="text" class="entrada" name="apellidos" disabled value="<?php echo $rs["apellidos"];?>"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Nombres</strong></p></td>
                        <td colspan="2"><input type="text" class="entrada" name="nombres" disabled value="<?php echo $rs["nombres"];?>"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Nickname</strong></p></td>
                        <td><input type="text" class="entrada" name="nickname" disabled value="<?php echo $rs["nickname"];?>"/></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Contraseña</strong></p></td>
                        <td colspan="3"><input type="password" name="contrasena" disabled value="<?php echo $rs["contrasena"];?>" class="entrada"></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Dirección</strong></p></td>
                        <td><input type="text" class="entrada" name="direccion" disabled value="<?php echo $rs["direccion"];?>" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Teléfono</strong></p></td>
                        <td><input type="text" class="entrada" name="telefono" disabled value="<?php echo $rs["telefono"];?>"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Empresa</strong></p></td>
                        <td colspan="3"><input type="text" name="empresa" disabled value="<?php echo $rs["empresa"];?>" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>E-mail</strong></p></td>
                        <td colspan="3"><input type="text" name="email" disabled value="<?php echo $rs["email"];?>" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Tipo de Usuario</strong></p></td>
                        <td align="center"><input type="text" name="tipo" disabled value="<?php echo $rs["tipo"];?>" class="entrada" size="1"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
						<td><input type="button" value="Cancelar" class="enviarBoton" onclick="window.location='./administrador.php?opc=11';"/></td>
                        <td><input type="button" value="Eliminar" class="enviarBoton" onclick="confirmar(this.form);"/></td>
						
                    </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
	$this->eliminar();
    }

//------------------------------------------------------

	private function eliminar(){
		if(isset($_POST['id_oculto'])){
			
			$id = $_POST["id_oculto"];

            
			$sSQL = "DELETE FROM usuarios WHERE id_usuario=? LIMIT 1;";

            $conn = $this->conectaDB();

            try{
                $stm = $conn->prepare( $sSQL );
                $stm->bindValue(1, $id); 

                $ok = $stm->execute();
                
                echo "<script>window.location='./administrador.php?opc=11';</script>";
                }
            catch ( PDOException $e ) {
            echo "Error de consulta" . $e->getMessage();
            }
	    }	
	}
}

?>