<?php
class mantenimientos{
    function __construct(){

    }

    function __destruct(){

    }

//------------------------------------------------------

    private function conectaDB() {
		$dbase = 'sims';
		$user = 'rootsims';
		$pass = '12345';
		$dsn = "mysql:dbname=".$dbase.";host=localhost";
		
		try {
			$link = new PDO( $dsn, $user, $pass );
			return $link;
		}
		catch ( PDOException $e ) {
			echo "Error de conexion a DB" . $e->getMessage();
		}
	}

//------------------------------------------------------

    public function listadomantenimientos($id_equipo){
?>
    <article id="listadoAr" >
        <h4 id="titListado">Listado de Mantenimientos</h4>
        <table border="1" id="tablaLis">
                <tr>
                    <th colspan="3" width="1">
                        <?php
                        if(isset($_SESSION['ctrlogA']) && ($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr")){
                        ?>

                        <input class="registroN" name="nuevoReg" type="button" value="Nuevo Mantenimiento" onclick="window.location='./administrador.php?opc=32';"/>
                        <?php
                        }
                        ?>
                    </th>
                    <th>Id Equipo</th>
                    <th>Id Usuario</th>
                    <th>Nombres</th>
                    <th>Fecha</th>
                </tr>
<?php

        if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
            $sSQL = "SELECT m.id_equipo,m.id_usuario,u.nombres,u.apellidos,m.fecha FROM mantenimientos m ";
            $sSQL .="INNER JOIN usuarios u ON u.id_usuario=m.id_usuario INNER JOIN equipos e ON e.id_equipo=m.id_equipo WHERE e.id_equipo=?;";
        }
        else{
            $sSQL = "SELECT m.id_equipo,m.id_usuario,u.nombres,u.apellidos,m.fecha FROM mantenimientos m ";
            $sSQL .="INNER JOIN usuarios u ON u.id_usuario=m.id_usuario;";
        }

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
        if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
            $stm->bindValue(1, $id_equipo); //si hubiera un filtrado
        }
            $ok = $stm->execute();
            $fila=0;
            while($rs = $stm->fetch(PDO::FETCH_ASSOC)){
                if(($fila%2)==0) {echo "<tr>";}
                else{echo "<tr class='par'>";}
                
                if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
?>
                <td colspan="3" align="center"><img src="../recursos/b_search.png" style="cursor:pointer;" onclick="window.location='./tecnico.php?opc=33&idu=<?php echo $rs['id_usuario'];?>&ide=<?php echo $rs['id_equipo'];?>&fe=<?php echo $rs['fecha'];?>';" /></td>
<?php
                }
                else if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
?>

                <td height="20" align="center"><img src="../recursos/b_edit.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=34&idu=<?php echo $rs['id_usuario'];?>&ide=<?php echo $rs['id_equipo'];?>&fe=<?php echo $rs['fecha'];?>';" /></td>
                <td align="center"><img src="../recursos/b_drop.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=35&idu=<?php echo $rs['id_usuario'];?>&ide=<?php echo $rs['id_equipo'];?>&fe=<?php echo $rs['fecha'];?>';" /></td>
                <td align="center"><img src="../recursos/b_search.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=33&idu=<?php echo $rs['id_usuario'];?>&ide=<?php echo $rs['id_equipo'];?>&fe=<?php echo $rs['fecha'];?>';" /></td>
<?php
                }
?>
                <td align="center"><?php echo $rs["id_equipo"];?></td>
                <td align="center"><?php echo $rs["id_usuario"];?></td>
                <td align="center"><?php echo $rs["nombres"]." ".$rs["apellidos"];?></td>
                <td align="center"><?php echo $rs["fecha"];?></td>
<?php
                
                 echo "</tr>";
                 $fila++;
            }


        }
        catch ( PDOException $e ) {
            echo "Error de consulta" . $e->getMessage();
        }

?>

            
        </table>
    </article>
<?php



    }

//------------------------------------------------------

    public function fagregarmantenimientos($id){
        $sSQL = "SELECT id_equipo FROM equipos WHERE id_equipo=?;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id); 

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);
        
?>
                <article id="artForm">
                    <h4 id="tForm">Registrar Mantenimientos</h4>
                    <form action="" method="POST">
                        <table>
                            <tr>
                                <td align="right"><p><strong>Id de Equipo</strong></p></td>
                                <?php
                                if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
                                    ?>
                                    <td><input type="text" name="id_equip" value="<?php echo $rs['id_equipo'];?>" disabled class="entrada">
                                    <input type="hidden" name="id_equipo" value="<?php echo $rs['id_equipo'];?>" class="entrada"></td>
                                    <?php
                                }
                                else if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
                                    ?>
                                    <td><input type="text" name="id_equipo" value="" class="entrada"></td>
                                    <?php
                                }
                                ?>
                                
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Id de Usuario</strong></p></td>
                                <td colspan="2"><input type="text" class="entrada" name="id_usuario" value="<?php echo $_SESSION['id'];?>" disabled>
                                <input type="hidden" class="entrada" name="id_oculto" value="<?php echo $_SESSION['id'];?>"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Fecha</strong></p></td>
                                <td colspan="2"><input type="date" class="entrada" name="fecha" value=""/></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Parte</strong></p></td>
                                <td><input type="text" class="entrada" name="parte" value=""/></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Descripción</strong></p></td>
                                <td colspan="3"><textarea style="height:100px;" type="text" name="descripcion" value="" class="entrada"></textarea></td>
                            </tr>
                            <tr>
                                <td align="right"><p><strong>Observación</strong></p></td>
                                <td colspan="3"><textarea style="height:100px;" type="text" name="observacion" value="" class="entrada"></textarea></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <?php 
                                if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
                                ?>
                                <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./tecnico.php?opc=21';"/></td>
                                <?php
                                }
                                else if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
                                ?>
                                <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./administrador.php?opc=31';"/></td>
                                <?php
                                }
                                ?>
                                <td><input type="submit" name="enviarf" value="Registrar" class="enviarBoton"/></td>
                            </tr>
                        </table>
                    </form>
                </article>
<?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
        $this->agregarmantenimientos();
    }

//------------------------------------------------------

    private function agregarmantenimientos(){

        if(isset($_POST['enviarf'])){
            $id_equipo      = $_POST['id_equipo'];
            $id_usuario     = $_POST['id_oculto'];
            $fecha          = $_POST['fecha'];
            $parte          = $_POST['parte'];
            $descripcion    = $_POST['descripcion'];
            $observacion    = $_POST['observacion'];
            

            $sSQL = "INSERT INTO mantenimientos VALUES(?, ?, ?, ?, ?, ?);";

            $conn = $this->conectaDB();

            try{
                $stm = $conn->prepare( $sSQL );

                $stm->bindValue(1, $id_equipo);
                $stm->bindValue(2, $id_usuario);
                $stm->bindValue(3, $fecha);
                $stm->bindValue(4, $parte);
                $stm->bindValue(5, $descripcion);
                $stm->bindValue(6, $observacion);
           

                $ok = $stm->execute();

                if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
                    echo "<script>window.location='./administrador.php?opc=31';</script>";
                }
                else if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
                    echo "<script>window.location='./tecnico.php?opc=21';</script>";
                }

            }
            catch ( PDOException $e ) {
                echo "Error de consulta" . $e->getMessage();
            }
        }


    }

//------------------------------------------------------

    public function fmodificarmantenimientos($idu, $ide, $fe){
         $sSQL = "SELECT * FROM mantenimientos WHERE id_usuario=? AND id_equipo=? AND fecha=? LIMIT 1;"; //LIMIT 1

         $conn = $this->conectaDB();

         try{
             $stm = $conn->prepare( $sSQL );
             $stm->bindValue(1, $idu);
             $stm->bindValue(2, $ide);
             $stm->bindValue(3, $fe);



             $ok = $stm->execute();
             $rs = $stm->fetch(PDO::FETCH_ASSOC);
    ?>
	
	
        <article id="artForm">
            <h4 id="tForm">Modificar Mantenimientos</h4>
            <form action="" method="POST">
                <table>
                <tr>
                    <td align="right"><p><strong>Id de Equipo</strong></p></td>
                    <td><input disabled type="text" name="id_equipo" value="<?php echo $rs["id_equipo"];?>" class="entrada">
                    <input type="hidden" name="id_oculto" value="<?php echo $rs["id_equipo"];?>" class="entrada"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Id de Usuario</strong></p></td>
                    <td colspan="2"><input disabled type="text" class="entrada" name="id_usuario" value="<?php echo $rs["id_usuario"];?>">
                    <input type="hidden" class="entrada" name="id_oculto2" value="<?php echo $rs["id_usuario"];?>"></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Fecha</strong></p></td>
                    <td colspan="2"><input type="date" class="entrada" name="fecha" value="<?php echo $rs["fecha"];?>"/></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Parte</strong></p></td>
                    <td><input type="text" class="entrada" name="parte" value="<?php echo $rs["r_parte"];?>"/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Descripción</strong></p></td>
                    <td colspan="3"><textarea style="height:100px;" type="text" name="descripcion" value="" class="entrada"><?php echo $rs["descripcion"];?></textarea></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Observación</strong></p></td>
                    <td colspan="3"><textarea style="height:100px;" type="text" name="observacion" value="" class="entrada"><?php echo $rs["observacion"];?></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./administrador.php?opc=31';"/></td>
                    <td><input type="submit" name="enviarf" value="Modificar" class="enviarBoton"/></td>
                </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
     catch ( PDOException $e ) {
         echo "Error de consulta" . $e->getMessage();
     }
	$this->modificarmantenimientos();
    }
	
//------------------------------------------------------

	private function modificarmantenimientos(){
        if(isset($_POST['enviarf'])){
            $id_equipo          = $_POST['id_oculto'];
            $id_usuario         = $_POST['id_oculto2'];
            $fecha              = $_POST['fecha'];
            $parte              = $_POST['parte'];
            $descripcion        = $_POST['descripcion'];
            $observacion        = $_POST['observacion'];

            $sSQL = "UPDATE mantenimientos SET fecha=?,r_parte=?,descripcion=?,observacion=? ";
			$sSQL .= "WHERE id_usuario=? && id_equipo=? LIMIT 1;";

            $conn = $this->conectaDB();

            try{
                $stm = $conn->prepare( $sSQL );
                
                $stm->bindValue(1, $fecha);
                $stm->bindValue(2, $parte);
                $stm->bindValue(3, $descripcion);
                $stm->bindValue(4, $observacion);
                $stm->bindValue(5, $id_usuario);
                $stm->bindValue(6, $id_equipo);

                

                $ok = $stm->execute();

                echo "<script>window.location='./administrador.php?opc=31';</script>";

            }
            catch ( PDOException $e ) {
                echo "Error de consulta" . $e->getMessage();
            }
        }
	}

//------------------------------------------------------

    public function consultarmantenimientos($idu, $ide, $fe){

        $sSQL = "SELECT * FROM mantenimientos WHERE id_usuario=? AND id_equipo=? AND fecha=? LIMIT 1;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );

            $stm->bindValue(1, $idu);
            $stm->bindValue(2, $ide);
            $stm->bindValue(3, $fe);

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);


    ?>
        <article id="artForm">
            <h4 id="tForm">Consultar Mantenimientos</h4>
            <form action="" method="POST">
                <table>
                    <tr>
                        <td align="right"><p><strong>Id de Equipo</strong></p></td>
                        <td><input disabled type="text" name="id_equipo" value="<?php echo $rs["id_equipo"];?>" class="entrada">
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Id de Usuario</strong></p></td>
                        <td colspan="2"><input disabled type="text" class="entrada" name="id_usuario" value="<?php echo $rs["id_usuario"];?>">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Fecha</strong></p></td>
                        <td colspan="2"><input disabled type="date" class="entrada" name="fecha" value="<?php echo $rs["fecha"];?>"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Parte</strong></p></td>
                        <td><input disabled type="text" class="entrada" name="parte" value="<?php echo $rs["r_parte"];?>"/></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Descripción</strong></p></td>
                        <td colspan="3"><textarea style="height:100px;" disabled type="text" name="descripcion" class="entrada"><?php echo $rs["descripcion"];?></textarea></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Observación</strong></p></td>
                        <td colspan="3"><textarea style="height:100px;" disabled type="text" name="observacion" value="" class="entrada"><?php echo $rs["observacion"];?></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <?php
                        if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
                        ?>
                            <td><input type="reset" value="Regresar" class="enviarBoton" onclick="window.location='./tecnico.php?opc=31&id=<?php echo $rs['id_equipo']; ?>';"/></td>

                        <?php
                        }
                        else{
                            ?> 
                        <td><input type="reset" value="Regresar" class="enviarBoton" onclick="window.location='./administrador.php?opc=31';"/></td>
                        <?php   
                    }
                    ?> 
                    </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
    }

//------------------------------------------------------

    public function feliminarmantenimientos($idu, $ide, $fe){
	$sSQL = "SELECT * FROM mantenimientos WHERE id_usuario=? AND id_equipo=? AND fecha=? LIMIT 1;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $idu);
            $stm->bindValue(2, $ide);
            $stm->bindValue(3, $fe);




            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);


    ?>
	
	<script>
	function confirmarmantenimientos(formulario){
		var res = window.confirm("¿Realmente desea eliminarlo?");
		
		if(res){
			formulario.submit();
		} 
		else{
			window.location='./administrador.php?opc=31';
		}
	}	
	</script>
        <article id="artForm">
            <h4 id="tForm">Eliminar Mantenimientos</h4>
            <form action="" method="POST">
                <table>
                <tr>
                    <td align="right"><p><strong>Id de Equipo</strong></p></td>
                    <td><input disabled type="text" name="id_equipo" value="<?php echo $rs["id_equipo"];?>" class="entrada">
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Id de Usuario</strong></p></td>
                    <td colspan="2"><input disabled type="text" class="entrada" name="id_usuario" value="<?php echo $rs["id_usuario"];?>">
                    <input type="hidden" name="id_oculto" value="<?php echo $rs["id_usuario"];?>" class="entrada">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Fecha</strong></p></td>
                    <td colspan="2"><input disabled type="date" class="entrada" name="fecha" value="<?php echo $rs["fecha"];?>"/></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Parte</strong></p></td>
                    <td><input disabled type="text" class="entrada" name="parte" value="<?php echo $rs["r_parte"];?>"/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Descripción</strong></p></td>
                    <td colspan="3"><textarea style="height:100px;" disabled type="text" name="descripcion" value="" class="entrada"><?php echo $rs["descripcion"];?></textarea></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Observación</strong></p></td>
                    <td colspan="3"><textarea style="height:100px;" disabled type="text" name="observacion" value="" class="entrada"><?php echo $rs["observacion"];?></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><input type="reset" value="Regresar" class="enviarBoton" onclick="window.location='./administrador.php?opc=31';"/></td>
                    <td><input type="button" value="Eliminar" class="enviarBoton" onclick="confirmarmantenimientos(this.form);"/></td>
                </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
	$this->eliminarmantenimientos();
    }
	
//------------------------------------------------------

	private function eliminarmantenimientos(){
		if(isset($_POST['id_oculto'])){
			
			$id = $_POST["id_oculto"];
			$sSQL = "DELETE FROM mantenimientos WHERE id_usuario=? LIMIT 1;";

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id); 

            $ok = $stm->execute();
			
			echo "<script>window.location='./administrador.php?opc=31';</script>";
			}
        catch ( PDOException $e ) {
            echo "Error de consulta" . $e->getMessage();
            }
	    }
    }
}

?>