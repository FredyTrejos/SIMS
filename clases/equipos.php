<?php
class equipos{
    function __construct(){

    }

    function __destruct(){

    }

//------------------------------------------------------

    private function conectaDB() {
		$dbase = 'sims';
		$user = 'rootsims';
		$pass = '12345';
		$dsn = "mysql:dbname=".$dbase.";host=localhost";
		
		try {
			$link = new PDO( $dsn, $user, $pass );
			return $link;
		}
		catch ( PDOException $e ) {
			echo "Error de conexion a DB" . $e->getMessage();
		}
	}

//------------------------------------------------------

    public function listadoequipos(){
?>
    <article id="listadoAr" >
        <h4 id="titListado">Listado de Equipos</h4>
        <table border="1" id="tablaLis">
                <tr>
                    
                        <?php
                        if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
                        ?>
                    <th colspan="3" width="1">
                        <input class="registroN" name="nuevoReg" type="button" value="Nuevo Equipo" onclick="window.location='./administrador.php?opc=22';"/>
                    </th>
                        
                   <?php
                        } else{
                            ?>
                    <th colspan="3" width="1">
                        <input class="registroN" name="nuevoReg" type="button" value="Nuevo Equipo" onclick="window.location='./tecnico.php?opc=22';"/>

                    </th>
                            <?php

                        }
                        ?>

                    <th>Id Equipo</th>
                    <th>Mainboard</th>
                    <th>Procesador</th>
                    <th>Ram</th>
                    <th>Video</th>
                </tr>
<?php
        $sSQL = "SELECT * FROM equipos;";

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            //$stm->bindValue(1, $var); si hubiera un filtrado

            $ok = $stm->execute();
            $fila=0;
            while($rs = $stm->fetch(PDO::FETCH_ASSOC)){
                if(($fila%2)==0) {echo "<tr>";}
                else{echo "<tr class='par'>";}
                
?>              
<?php
                if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
?>
                <td align="center"><img src="../recursos/b_edit.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=24&id=<?php echo $rs['id_equipo']; ?>';" /></td>
                <td align="center"><img class="iconorep" src="../recursos/b_drop.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=25&id=<?php echo $rs['id_equipo']; ?>';" /></td>
                <td align="center"><img src="../recursos/b_search.png" style="cursor:pointer;" onclick="window.location='./administrador.php?opc=23&id=<?php echo $rs['id_equipo']; ?>';" /></td>
<?php                
                }  
                else if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
            ?>
                <td align="center"><img class="iconorep" src="../recursos/b_view.png" style="cursor:pointer;" onclick="window.location='./tecnico.php?opc=31&id=<?php echo $rs['id_equipo']; ?>';" /></td>
                <td align="center"><img class="iconorep" src="../recursos/b_mant.png" style="cursor:pointer;" onclick="window.location='./tecnico.php?opc=32&id=<?php echo $rs['id_equipo']; ?>';" /></td>
                <td align="center"><img src="../recursos/b_search.png" style="cursor:pointer;" onclick="window.location='./tecnico.php?opc=23&id=<?php echo $rs['id_equipo']; ?>';" /></td>
            <?php    
            }
            ?>     
                <td><?php echo $rs["id_equipo"];?></td>
                <td><?php echo $rs["mainboard"];?></td>
                <td><?php echo $rs["procesador"];?></td>
                <td><?php echo $rs["ram"];?></td>
                <td><?php echo $rs["video"];?></td>
<?php
                 echo "</tr>";
                 $fila++;
            }


        }
        catch ( PDOException $e ) {
            echo "Error de consulta" . $e->getMessage();
        }

?>

            
        </table>
    </article>
<?php



    }

//------------------------------------------------------

    public function fagregarequipos(){
       
?>
        <article id="artForm">
            <h4 id="tForm">Registrar Equipos</h4>
            <form action="" method="POST">
                <table>
                    <tr>
                        <td align="right"><p><strong>Identificación</strong></p></td>
                        <td><input type="text" name="id_equipo" value="" class="entrada"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Mainboard</strong></p></td>
                        <td colspan="2"><input type="text" class="entrada" name="mainboard" value=""></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Procesador</strong></p></td>
                        <td colspan="2"><input type="text" class="entrada" name="procesador" value=""/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Ram</strong></p></td>
                        <td><input type="text" class="entrada" name="ram" value=""/></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>Video</strong></p></td>
                        <td colspan="3"><input type="text" name="video" value="" class="entrada"></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>PCI1</strong></p></td>
                        <td><input type="text" class="entrada" name="pci1" value="" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>PCI2</strong></p></td>
                        <td><input type="text" class="entrada" name="pci2" value=""></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right"><p><strong>PCI3</strong></p></td>
                        <td><input type="text" name="pci3" value="" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Fuente</strong></p></td>
                        <td colspan="3"><input type="text" name="fuente" value="" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>HDD</strong></p></td>
                        <td><input type="text" name="hdd" value="" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>DVD</strong></p></td>
                        <td><input type="text" name="dvd" value="" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Gabinete</strong></p></td>
                        <td colspan="3"><input type="text" name="gabinete" value="" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Teclado</strong></p></td>
                        <td colspan="2"><input type="text" name="teclado" value="" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Mouse</strong></p></td>
                        <td colspan="2"><input type="text" name="mouse" value="" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Monitor</strong></p></td>
                        <td colspan="2"><input type="text" name="monitor" value="" class="entrada"></td>

                    </tr>
                    <tr>
                        <td align="right"><p><strong>Otros</strong></p></td>
                        <td colspan="2"><input type="text" name="otros" value="" class="entrada"></td>
                    </tr>

                    
                    <tr>
                        <td></td>
                        <td></td>
                        <?php
                        if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
                        ?>
                        <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./administrador.php?opc=21';"/></td>
                        <?php
                        }
                        else{
                            ?>
                            <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./tecnico.php?opc=21';"/></td>
                            <?php
                        }
                        ?>
                        <td><input type="submit" name="enviarf" value="Registrar" class="enviarBoton"/></td>
                    </tr>
                </table>
            </form>
        </article>
<?php
        $this->agregarequipos();
    }

//------------------------------------------------------

    private function agregarequipos(){

        if(isset($_POST['enviarf'])){
            $id_equipo      = $_POST['id_equipo'];
            $mainboard      = $_POST['mainboard'];
            $procesador     = $_POST['procesador'];
            $ram            = $_POST['ram'];
            $video          = $_POST['video'];
            $pci1           = $_POST['pci1'];
            $pci2           = $_POST['pci2'];
            $pci3           = $_POST['pci3'];
            $fuente         = $_POST['fuente'];
            $hdd            = $_POST['hdd'];
            $dvd            = $_POST['dvd'];
            $gabinete       = $_POST['gabinete'];
            $teclado        = $_POST['teclado'];
            $mouse          = $_POST['mouse'];
            $monitor        = $_POST['monitor'];
            $otros          = $_POST['otros'];
            

            $sSQL = "INSERT INTO equipos VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            $conn = $this->conectaDB();

            try{
                $stm = $conn->prepare( $sSQL );

                $stm->bindValue(1, $id_equipo);
                $stm->bindValue(2, $mainboard);
                $stm->bindValue(3, $procesador);
                $stm->bindValue(4, $ram);
                $stm->bindValue(5, $video);
                $stm->bindValue(6, $pci1);
                $stm->bindValue(7, $pci2);
                $stm->bindValue(8, $pci3);
                $stm->bindValue(9, $fuente);
                $stm->bindValue(10, $hdd);
                $stm->bindValue(11, $dvd);
                $stm->bindValue(12, $gabinete);
                $stm->bindValue(13, $teclado);
                $stm->bindValue(14, $mouse);
                $stm->bindValue(15, $monitor);
                $stm->bindValue(16, $otros);             

                $ok = $stm->execute();

                if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
                echo "<script>window.location='./administrador.php?opc=21';</script>";
                }
                else{
                echo "<script>window.location='./tecnico.php?opc=21';</script>";

                }
            }
            catch ( PDOException $e ) {
                echo "Error de consulta" . $e->getMessage();
            }
        }


    }

//------------------------------------------------------

    public function fmodificarequipos($id_equipo){
        $sSQL = "SELECT * FROM equipos WHERE id_equipo=?;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id_equipo); 

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);
    ?>
	
	
        <article id="artForm">
            <h4 id="tForm">Modificar Equipos</h4>
            <form action="" method="POST">
                <table>
                <tr>
                    <td align="right"><p><strong>Identificación</strong></p></td>
                    <td><input disabled type="text" name="id_equipo" value="<?php echo $rs["id_equipo"];?>" class="entrada">
                    <input type="hidden" name="id_oculto" value="<?php echo $rs["id_equipo"];?>" class="entrada"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Mainboard</strong></p></td>
                    <td colspan="2"><input type="text" class="entrada" name="mainboard" value="<?php echo $rs["mainboard"];?>"></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Procesador</strong></p></td>
                    <td colspan="2"><input type="text" class="entrada" name="procesador" value="<?php echo $rs["procesador"];?>"/></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Ram</strong></p></td>
                    <td><input type="text" class="entrada" name="ram" value="<?php echo $rs["ram"];?>"/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Video</strong></p></td>
                    <td colspan="3"><input type="text" name="video" value="<?php echo $rs["video"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI1</strong></p></td>
                    <td><input type="text" class="entrada" name="pci1" value="<?php echo $rs["pci1"];?>" /></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI2</strong></p></td>
                    <td><input type="text" class="entrada" name="pci2" value="<?php echo $rs["pci2"];?>"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI3</strong></p></td>
                    <td><input type="text" name="pci3" value="<?php echo $rs["pci3"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Fuente</strong></p></td>
                    <td colspan="3"><input type="text" name="fuente" value="<?php echo $rs["fuente"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>HDD</strong></p></td>
                    <td><input type="text" name="hdd" value="<?php echo $rs["hdd"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>DVD</strong></p></td>
                    <td><input type="text" name="dvd" value="<?php echo $rs["dvd"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Gabinete</strong></p></td>
                    <td colspan="3"><input type="text" name="gabinete" value="<?php echo $rs["gabinete"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Teclado</strong></p></td>
                    <td colspan="2"><input type="text" name="teclado" value="<?php echo $rs["teclado"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Mouse</strong></p></td>
                    <td colspan="2"><input type="text" name="mouse" value="<?php echo $rs["mouse"];?>" class="entrada"></td>

                </tr>
                <tr>
                    <td align="right"><p><strong>Monitor</strong></p></td>
                    <td colspan="2"><input type="text" name="monitor" value="<?php echo $rs["monitor"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Otros</strong></p></td>
                    <td colspan="2"><input type="text" name="otros" value="<?php echo $rs["otros"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./administrador.php?opc=21';"/></td>
                    <td><input type="submit" name="enviarf" value="Modificar" class="enviarBoton"/></td>
                </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
	$this->modificarequipos();
    }

//------------------------------------------------------

	private function modificarequipos(){
        if(isset($_POST['enviarf'])){
            $id_equipo      = $_POST['id_oculto'];
            $mainboard      = $_POST['mainboard'];
            $procesador     = $_POST['procesador'];
            $ram            = $_POST['ram'];
            $video          = $_POST['video'];
            $pci1           = $_POST['pci1'];
            $pci2           = $_POST['pci2'];
            $pci3           = $_POST['pci3'];
            $fuente         = $_POST['fuente'];
            $hdd            = $_POST['hdd'];
            $dvd            = $_POST['dvd'];
            $gabinete       = $_POST['gabinete'];
            $teclado        = $_POST['teclado'];
            $mouse          = $_POST['mouse'];
            $monitor        = $_POST['monitor'];
            $otros          = $_POST['otros'];

            $sSQL = "UPDATE equipos SET mainboard=?,procesador=?,ram=?,video=?,pci1=?,pci2=?,pci3=?,fuente=?,hdd=?,dvd=?,gabinete=?,teclado=?,mouse=?,monitor=?,otros=? ";
			$sSQL .= "WHERE id_equipo=? LIMIT 1;";

            $conn = $this->conectaDB();

            try{
                $stm = $conn->prepare( $sSQL );
                
                $stm->bindValue(1, $mainboard);
                $stm->bindValue(2, $procesador);
                $stm->bindValue(3, $ram);
                $stm->bindValue(4, $video);
                $stm->bindValue(5, $pci1);
                $stm->bindValue(6, $pci2);
                $stm->bindValue(7, $pci3);
                $stm->bindValue(8, $fuente);
                $stm->bindValue(9, $hdd);
                $stm->bindValue(10, $dvd);
                $stm->bindValue(11, $gabinete);
                $stm->bindValue(12, $teclado);
                $stm->bindValue(13, $mouse);
                $stm->bindValue(14, $monitor);
                $stm->bindValue(15, $otros); 
                $stm->bindValue(16, $id_equipo);
                

                $ok = $stm->execute();

                echo "<script>window.location='./administrador.php?opc=21';</script>";
            }
            catch ( PDOException $e ) {
                echo "Error de consulta" . $e->getMessage();
            }
        }
	}

//------------------------------------------------------

    public function consultarequipos($id_equipo){

        $sSQL = "SELECT * FROM equipos WHERE id_equipo=?;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id_equipo); 

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);


    ?>
        <article id="artForm">
            <h4 id="tForm">Consultar Equipos</h4>
            <form action="" method="POST">
                <table>
                <tr>
                    <td align="right"><p><strong>Identificación</strong></p></td>
                    <td><input disabled type="text" name="id_equipo" value="<?php echo $rs["id_equipo"];?>" class="entrada" /></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Mainboard</strong></p></td>
                    <td colspan="2"><input disabled type="text" class="entrada" name="mainboard" value="<?php echo $rs["mainboard"];?>" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Procesador</strong></p></td>
                    <td colspan="2"><input disabled type="text" class="entrada" name="procesador" value="<?php echo $rs["procesador"];?>" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Ram</strong></p></td>
                    <td><input disabled type="text" class="entrada" name="ram" value="<?php echo $rs["ram"];?>"/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Video</strong></p></td>
                    <td colspan="3"><input disabled type="text" name="video" value="<?php echo $rs["video"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI1</strong></p></td>
                    <td><input disabled type="text" class="entrada" name="pci1" value="<?php echo $rs["pci1"];?>" /></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI2</strong></p></td>
                    <td><input disabled type="text" class="entrada" name="pci2" value="<?php echo $rs["pci2"];?>"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI3</strong></p></td>
                    <td><input disabled type="text" name="pci3" value="<?php echo $rs["pci3"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Fuente</strong></p></td>
                    <td colspan="3"><input disabled type="text" name="fuente" value="<?php echo $rs["fuente"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>HDD</strong></p></td>
                    <td><input disabled type="text" name="hdd" value="<?php echo $rs["hdd"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>DVD</strong></p></td>
                    <td><input disabled type="text" name="dvd" value="<?php echo $rs["dvd"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Gabinete</strong></p></td>
                    <td colspan="3"><input disabled type="text" name="gabinete" value="<?php echo $rs["gabinete"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Teclado</strong></p></td>
                    <td colspan="2"><input disabled type="text" name="teclado" value="<?php echo $rs["teclado"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Mouse</strong></p></td>
                    <td colspan="2"><input disabled type="text" name="mouse" value="<?php echo $rs["mouse"];?>" class="entrada"></td>

                </tr>
                <tr>
                    <td align="right"><p><strong>Monitor</strong></p></td>
                    <td colspan="2"><input disabled type="text" name="monitor" value="<?php echo $rs["monitor"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Otros</strong></p></td>
                    <td colspan="2"><input disabled type="text" name="otros" value="<?php echo $rs["otros"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <!-- <td><input type="reset" value="Regresar" class="enviarBoton" onclick="window.location='./administrador.php?opc=21';"/></td> -->
                    <?php 
                        if($_SESSION['ctrlogT']=="@mifjdeiofjefij_28lkl*mn"){
                        ?>
                        <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./tecnico.php?opc=21';"/></td>
                        <?php
                        }
                        else if($_SESSION['ctrlogA']=="@itwharl4u28isa?_fifrvr"){
                        ?>
                        <td><input type="reset" value="Cancelar" class="enviarBoton" onclick="window.location='./administrador.php?opc=21';"/></td>
                        <?php
                        }
                        ?>
                </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
    }

//------------------------------------------------------

    public function feliminarequipos($id_equipo){
	$sSQL = "SELECT * FROM equipos WHERE id_equipo=?;"; //LIMIT 1

        $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id_equipo); 

            $ok = $stm->execute();
            $rs = $stm->fetch(PDO::FETCH_ASSOC);


    ?>
	
	<script>
	function confirmarequipos(formular){
		var res = window.confirm("¿Realmente desea eliminarlo?");
		
		if(res){
			formular.submit();
		} 
		else{
			window.location='./administrador.php?opc=21';
		}
	}
	
	</script>
        <article id="artForm">
            <h4 id="tForm">Eliminar Equipos</h4>
            <form action="" method="POST">
                <table>
                <tr>
                    <td align="right"><p><strong>Identificación</strong></p></td>
                    <td><input disabled type="text" name="id_equipo" value="<?php echo $rs["id_equipo"];?>" class="entrada" />
                    <input type="hidden" name="id_oculto" value="<?php echo $rs["id_equipo"];?>" class="entrada" /></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Mainboard</strong></p></td>
                    <td colspan="2"><input disabled type="text" class="entrada" name="mainboard" value="<?php echo $rs["mainboard"];?>" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Procesador</strong></p></td>
                    <td colspan="2"><input disabled type="text" class="entrada" name="procesador" value="<?php echo $rs["procesador"];?>" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Ram</strong></p></td>
                    <td><input disabled type="text" class="entrada" name="ram" value="<?php echo $rs["ram"];?>"/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Video</strong></p></td>
                    <td colspan="3"><input disabled type="text" name="video" value="<?php echo $rs["video"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI1</strong></p></td>
                    <td><input disabled type="text" class="entrada" name="pci1" value="<?php echo $rs["pci1"];?>" /></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI2</strong></p></td>
                    <td><input disabled type="text" class="entrada" name="pci2" value="<?php echo $rs["pci2"];?>"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>PCI3</strong></p></td>
                    <td><input disabled type="text" name="pci3" value="<?php echo $rs["pci3"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Fuente</strong></p></td>
                    <td colspan="3"><input disabled type="text" name="fuente" value="<?php echo $rs["fuente"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>HDD</strong></p></td>
                    <td><input disabled type="text" name="hdd" value="<?php echo $rs["hdd"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>DVD</strong></p></td>
                    <td><input disabled type="text" name="dvd" value="<?php echo $rs["dvd"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Gabinete</strong></p></td>
                    <td colspan="3"><input disabled type="text" name="gabinete" value="<?php echo $rs["gabinete"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Teclado</strong></p></td>
                    <td colspan="2"><input disabled type="text" name="teclado" value="<?php echo $rs["teclado"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Mouse</strong></p></td>
                    <td colspan="2"><input disabled type="text" name="mouse" value="<?php echo $rs["mouse"];?>" class="entrada"></td>

                </tr>
                <tr>
                    <td align="right"><p><strong>Monitor</strong></p></td>
                    <td colspan="2"><input disabled type="text" name="monitor" value="<?php echo $rs["monitor"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td align="right"><p><strong>Otros</strong></p></td>
                    <td colspan="2"><input disabled type="text" name="otros" value="<?php echo $rs["otros"];?>" class="entrada"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><input type="reset" value="Regresar" class="enviarBoton" onclick="window.location='./administrador.php?opc=21';"/></td>
                    <td><input type="button" value="Eliminar" class="enviarBoton" onclick="confirmarequipos(this.form);"/></td>
                    </tr>
                </table>
            </form>
        </article>
    
    <?php
    }
    catch ( PDOException $e ) {
        echo "Error de consulta" . $e->getMessage();
    }
	$this->eliminarequipos();
    }
	
//------------------------------------------------------

	private function eliminarequipos(){
		if(isset($_POST['id_oculto'])){
			
			$id = $_POST["id_oculto"];
			$sSQL = "DELETE FROM equipos WHERE id_equipo=? LIMIT 1;";

            $conn = $this->conectaDB();

        try{
            $stm = $conn->prepare( $sSQL );
            $stm->bindValue(1, $id); 

            $ok = $stm->execute();
			
			echo "<script>window.location='./administrador.php?opc=21';</script>";
			}
        catch ( PDOException $e ) {
            echo "Error de consulta" . $e->getMessage();
            }
	    }
	}
}

?>