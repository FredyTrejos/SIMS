-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-11-2023 a las 21:27:56
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sims`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id_equipo` varchar(15) NOT NULL,
  `mainboard` varchar(50) DEFAULT NULL,
  `procesador` varchar(50) DEFAULT NULL,
  `ram` varchar(50) DEFAULT NULL,
  `video` varchar(50) DEFAULT NULL,
  `pci1` varchar(50) DEFAULT NULL,
  `pci2` varchar(50) DEFAULT NULL,
  `pci3` varchar(50) DEFAULT NULL,
  `fuente` varchar(50) DEFAULT NULL,
  `hdd` varchar(50) DEFAULT NULL,
  `dvd` varchar(50) DEFAULT NULL,
  `gabinete` varchar(50) DEFAULT NULL,
  `teclado` varchar(50) DEFAULT NULL,
  `mouse` varchar(50) DEFAULT NULL,
  `monitor` varchar(50) DEFAULT NULL,
  `otros` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id_equipo`, `mainboard`, `procesador`, `ram`, `video`, `pci1`, `pci2`, `pci3`, `fuente`, `hdd`, `dvd`, `gabinete`, `teclado`, `mouse`, `monitor`, `otros`) VALUES
('12345', 'no sé', 'no se', 'no se', 'no se', 'no se', 'no se', 'no se', 'no se', 'no se', 'no se', 'no se no se', 'no se', 'no se', 'no se', 'no se'),
('54321', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea', 'ni idea');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimientos`
--

CREATE TABLE `mantenimientos` (
  `id_equipo` varchar(15) DEFAULT NULL,
  `id_usuario` varchar(15) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `r_parte` varchar(20) DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  `observacion` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `mantenimientos`
--

INSERT INTO `mantenimientos` (`id_equipo`, `id_usuario`, `fecha`, `r_parte`, `descripcion`, `observacion`) VALUES
('54321', '6666', '2023-11-13', 'no se', 'No sé que poner aquí', 'Tampoco aquí'),
('54321', '2222', '0000-00-00', 'ththth', 'fhthtrhth', 'ththth');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` varchar(15) NOT NULL,
  `tipo` varchar(2) DEFAULT NULL,
  `apellidos` varchar(25) DEFAULT NULL,
  `nombres` varchar(25) DEFAULT NULL,
  `nickname` varchar(15) DEFAULT NULL,
  `contrasena` varchar(40) DEFAULT NULL,
  `direccion` varchar(35) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `empresa` varchar(20) DEFAULT NULL,
  `email` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `tipo`, `apellidos`, `nombres`, `nickname`, `contrasena`, `direccion`, `telefono`, `empresa`, `email`) VALUES
('2222', 'A', 'Trejos', 'Fredy', 'SENASENA', 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'Dosquebradas', '3215569549', 'SENADSQ', 'jhoftrejos@gmail.com'),
('3333', 'A', 'Jhon', 'Duque', '', 'fe703d258c7ef5f50b71e06565a65aa07194907f', '', '', '', ''),
('4444', 'T', 'Montoya', 'Andres', '', 'fe703d258c7ef5f50b71e06565a65aa07194907f', '', '', 'SENAC', ''),
('6666', 'T', 'Angel', 'Miguel', 'aprendizsena', '', '', '', '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id_equipo`);

--
-- Indices de la tabla `mantenimientos`
--
ALTER TABLE `mantenimientos`
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_equipo` (`id_equipo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `mantenimientos`
--
ALTER TABLE `mantenimientos`
  ADD CONSTRAINT `mantenimientos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`),
  ADD CONSTRAINT `mantenimientos_ibfk_2` FOREIGN KEY (`id_equipo`) REFERENCES `equipos` (`id_equipo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
